#include "guiapplication.h"

// othello game library
#include <orangemonkey_ai.h>


// qt
#include <QQmlContext>
#include <QQuickItem>
#include <QQuickWindow>
#include <QTimer>
#include <QDebug>
#include <QThread>

// stl
#include <chrono>
using namespace std::chrono_literals;


GuiApplication::GuiApplication(int& argc, char** argv)
  : QGuiApplication(argc, argv),
    m_game_engine{}, m_model{m_game_engine}, m_app{}
{

  m_app.rootContext()->setContextProperty("gamemodel", &m_model);

  m_app.load(QUrl("qrc:/qml/gui.qml"));

  auto* root_window = qobject_cast<QQuickWindow*>(m_app.rootObjects().first());
  if (root_window) {

    connect(root_window, SIGNAL(endGameAndQuit()), this,
            SLOT(endGameAndQuit()));

    connect(root_window, SIGNAL(initNewHumanGame()), this,
            SLOT(initNewHumanGame()));

    connect(root_window, SIGNAL(initNewMonkeyGame()), this,
            SLOT(initNewMonkeyGame()));

    //connect(root_window, SIGNAL(boardClicked(int)), this,
            //SLOT(pieceClicked(int)));

    connect(root_window, SIGNAL(boardClicked(int)), this,
            SLOT(pieceClicked(int)));

    connect(this, SIGNAL(displayFinalScores(int, int)), root_window,
            SIGNAL(displayFinalScores(int, int)));

    connect(this, &GuiApplication::enqueueNextTurn, this,
               &GuiApplication::startNextTurn);

    connect(this, &GuiApplication::gameEnded, this,
            &GuiApplication::endOfGameActions);
  }
}

void GuiApplication::startNextTurn()
{
     m_model.update(); //just for humanvs human
     //check if the board is full
     std::cout<<othello::utility::occupiedPositions(m_game_engine.board()).count()<<std::endl;

     if(othello::utility::occupiedPositions(m_game_engine.board()).all())
     {
         EndGame();
     }
     if(othello::utility::legalMoves(m_game_engine.board(),m_game_engine.currentPlayerId()).empty())
     {
         if(noturnleft)
         {
             EndGame();
             return;
         }
         noturnleft = true;
         m_game_engine.SwapPlayer();
         emit(enqueueNextTurn());
         return;

     }
     else
     {
         noturnleft = false;
     }

    if(m_game_engine.currentPlayerType() == othello::PlayerType::Human)
        return;
    else
    {
        m_game_engine.think(3s);
        emit(enqueueNextTurn());
    } //all for aimonkey


}

void GuiApplication::EndGame()
{
    std::cout<<"Test";
    emit displayFinalScores(int(m_game_engine.board()[0].count()),int(m_game_engine.board()[1].count()));
}

void GuiApplication::initNewHumanGame()
{
    m_game_engine.initNewGame();
    m_game_engine.initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
    m_game_engine.initPlayerType<othello::HumanPlayer,othello::PlayerId::Two>();
    //m_model.update();
    emit enqueueNextTurn();
}


void GuiApplication::initNewMonkeyGame()
{
    //m_game_engine.initNewGame();
    m_game_engine.initPlayerType<othello::HumanPlayer,othello::PlayerId::One>();
    //m_game_engine.initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::Two>();//monkey vs monkey
    m_game_engine.initPlayerType<othello::monkey_ais::OrangeMonkeyAI,othello::PlayerId::Two>();
    m_game_engine.initNewGame(); //first initialize the game and then call monkeys

    m_model.update();
    //emit enqueueNextTurn(); //for human vs human
    emit enqueueNextTurn();
}

void GuiApplication::loopthrough()
{
    m_game_engine.think(2s); // think for 2 secs
    m_model.update();
    emit enqueueNextTurn();
}


void GuiApplication::endGameAndQuit() { QGuiApplication::quit(); }

void GuiApplication::pieceClicked(int piece)
{
    auto success = m_game_engine.performMoveForCurrentHuman(othello::BitPos(piece));
    //std::cout<<othello::utility::occupiedPositions(m_game_engine.board())<<std::endl;
    if(!success)
        return;
    emit enqueueNextTurn();
}
void GuiApplication::endOfGameActions()
{
}
